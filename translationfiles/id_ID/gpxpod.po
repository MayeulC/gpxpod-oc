msgid ""
msgstr ""
"Project-Id-Version: gpxpod\n"
"Report-Msgid-Bugs-To: translations\\@example.com\n"
"POT-Creation-Date: 2019-07-25 01:07+0200\n"
"PO-Revision-Date: 2019-07-24 23:09\n"
"Last-Translator: Julien Veyssier (eneiluj)\n"
"Language-Team: Indonesian\n"
"Language: id_ID\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: gpxpod\n"
"X-Crowdin-Language: id\n"
"X-Crowdin-File: /master/translationfiles/templates/gpxpod.pot\n"

#: /var/www/html/n16/apps/gpxpod/appinfo/app.php:47
#: /var/www/html/n16/apps/gpxpod/specialAppInfoFakeDummyForL10nScript.php:2
msgid "GpxPod"
msgstr "GpxPod"

#: /var/www/html/n16/apps/gpxpod/controller/pagecontroller.php:1565
msgid "There was an error during \"gpxelevations\" execution on the server"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/filetypes.js:51
#: /var/www/html/n16/apps/gpxpod/js/filetypes.js:106
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4433
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4470
msgid "Directory {p} has been added"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/filetypes.js:58
#: /var/www/html/n16/apps/gpxpod/js/filetypes.js:113
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4445
msgid "Failed to add directory"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/filetypes.js:67
#: /var/www/html/n16/apps/gpxpod/js/filetypes.js:121
msgid "View in GpxPod"
msgstr "Tampilkan dalam GpxPod"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:990
msgid "File"
msgstr "Berkas"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:991
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1483
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:3962
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4025
msgid "download"
msgstr "unduhan"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:996
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1501
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5076
msgid "This public link will work only if '{title}' or one of its parent folder is shared in 'files' app by public link without password"
msgstr "Tautan publik ini hanya akan bekerja jika '{title}' atau salah satu induk foldernya dibagi di app \"files\" oleh tautan publik tanpa kata kunci"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1003
msgid "Draw track"
msgstr "Menggambar berkas"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1008
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1011
msgid "metadata link"
msgstr "tautan metadata"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1015
msgid "tracks/routes name list"
msgstr "daftar nama jalur/rute"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1030
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:421
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:114
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:115
msgid "Distance"
msgstr "Jarak"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1040
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1614
msgid "Duration"
msgstr "Durasi"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1043
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:116
msgid "Moving time"
msgstr "Waktu bergerak"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1046
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:117
msgid "Pause time"
msgstr "Jeda waktu"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1066
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:123
msgid "Begin"
msgstr "Mulai"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1069
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:124
msgid "End"
msgstr "Selesai"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1072
#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:268
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:426
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:121
msgid "Cumulative elevation gain"
msgstr "Kenaikan elevasi kumulatif"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1076
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:122
msgid "Cumulative elevation loss"
msgstr "Penurunan elevasi kumulatif"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1080
msgid "Minimum elevation"
msgstr "Elevasi terendah"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1084
msgid "Maximum elevation"
msgstr "Ketinggian tertinggi"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1088
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:120
msgid "Maximum speed"
msgstr "Kecepatan tertinggi"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1099
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:119
msgid "Average speed"
msgstr "Kecepatan rata-rata"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1110
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:118
msgid "Moving average speed"
msgstr "Kecepatan bergerak rata-rata"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1120
msgid "Moving average pace"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1136
msgid "{n} track"
msgid_plural "{n} tracks"
msgstr[0] ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1137
msgid "{np} picture"
msgid_plural "{np} pictures"
msgstr[0] ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1200
msgid "Track \"{tn}\" is loading"
msgstr "Berkas \"{tn}\" sedang dimuat"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1294
msgid "Failed to delete track"
msgstr "Gagal menghapus berkas"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1295
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1349
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1376
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4245
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4387
msgid "Reload this page"
msgstr "Muat ulang halaman ini"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1297
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1351
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1378
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4247
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4299
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4378
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4389
msgid "Error"
msgstr "Kesalahan"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1309
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1363
msgid "Successfully deleted"
msgstr "Berhasil dihapus"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1311
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1365
msgid "You can restore deleted files in \"Files\" app"
msgstr "Anda dapat mengembalikan berkas-berkas yang terhapus di app \"Files\""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1315
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1369
msgid "Impossible to delete"
msgstr "Mustahil untuk menghapus"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1320
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1348
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1375
msgid "Failed to delete selected tracks"
msgstr "Gagal menghapus berkas yang terpilih"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1441
msgid "Click the color to change it"
msgstr "Klik warna untuk mengubahnya"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1443
msgid "Deselect to hide track drawing"
msgstr "Batalkan pilihan untuk menyembunyikan berkas penggambaran"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1449
msgid "Select to draw the track"
msgstr "Pilih untuk menggambar berkas"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1491
msgid "More"
msgstr "Lainnya"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1495
msgid "Center map on this track"
msgstr "Peta pusat di berkas ini"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1511
msgid "Delete this track file"
msgstr "Hapus berkas ini"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1517
msgid "Correct elevations with smoothing for this track"
msgstr "Perbaiki ketinggian dengan menghaluskan berkas ini"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1522
msgid "Correct elevations for this track"
msgstr "Ketinggian yang benar untuk berkas ini"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1531
msgid "View this file in GpxMotion"
msgstr "Lihat berkas ini di GpxMotion"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1540
msgid "Edit this file in GpxEdit"
msgstr "Edit berkas ini di GpxEdit"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1549
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:3818
msgid "no date"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1581
msgid "No track visible"
msgstr "Tidak ada berkas yang terlihat"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1586
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4886
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4899
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:288
msgid "Tracks from current view"
msgstr "Berkas dari tampilan aktif"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1589
msgid "All tracks"
msgstr "Semua berkas"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1605
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:170
msgid "Draw"
msgstr "Gambar"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1607
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1863
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2698
#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:198
msgid "Track"
msgstr "Berkas"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1609
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:416
msgid "Date"
msgstr "Tanggal"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1611
msgid "Dist<br/>ance<br/>"
msgstr "Dist<br/>ance<br/>"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1616
msgid "Cumulative<br/>elevation<br/>gain"
msgstr "Komulatif<br/>ketinggian<br/>kenaikan"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1722
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:74
msgid "distance"
msgstr "jarak"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1724
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2598
msgid "altitude/distance"
msgstr "ketinggian/jarak"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1727
msgid "speed/distance"
msgstr "kecepatan/jarak"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1730
msgid "pace(time for last km or mi)/distance"
msgstr "kecepatan(waktu terakhir untuk km atau mi)/jarak"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1866
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2701
msgid "Link"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1869
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2704
#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:322
msgid "Elevation"
msgstr "Ketinggian"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1872
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2707
msgid "Latitude"
msgstr "Garis lintang"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1873
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2708
msgid "Longitude"
msgstr "Garis bujur"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1876
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2080
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2384
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2711
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2775
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2936
msgid "Comment"
msgstr "Komentar"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1880
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2715
msgid "Description"
msgstr "Keterangan"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1884
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2719
msgid "Symbol name"
msgstr "Nama simbol/lambang"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:3251
msgid "Database has been cleaned"
msgstr "Database telah dibersihkan"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:3254
msgid "Impossible to clean database"
msgstr "Mustahil untuk membersihkan database"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:3372
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:20
msgid "Folder"
msgstr "Folder"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:3384
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5129
msgid "Public link to '{folder}' which will work only if this folder is shared in 'files' app by public link without password"
msgstr "Link umum untuk '{folder}' dimana hanya akan bekerja jika folder ini dibagikan dalam bentuk app 'files' oleh link umum tanpa kata sandi"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:3960
msgid "Public folder share"
msgstr "Membagikan folder umum"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4023
msgid "Public file share"
msgstr "Membagikan berkas umum"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4071
msgid "Server name or server url should not be empty"
msgstr "Nama server atau url server tidak harus kosong"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4072
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4077
msgid "Impossible to add tile server"
msgstr "Mustahil untuk menambahkan server ubin"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4076
msgid "A server with this name already exists"
msgstr "Server dengan nama ini sudah pernah ada"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4109
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:465
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:509
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:554
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:603
msgid "Delete"
msgstr "Hapus"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4142
msgid "Tile server \"{ts}\" has been added"
msgstr "Server ubin \"{ts}\" telah ditambahkan"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4145
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4149
msgid "Failed to add tile server \"{ts}\""
msgstr "Gagal untuk menambahkan server ubin \"{ts}\""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4183
msgid "Tile server \"{ts}\" has been deleted"
msgstr "Tile server \" {ts} \" telah dihapus"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4186
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4190
msgid "Failed to delete tile server \"{ts}\""
msgstr "Gagal untuk menghapus server ubin \"{ts}\""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4244
msgid "Failed to restore options values"
msgstr "Gagal untuk mengembalikan opsi nilai"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4298
msgid "Failed to save options values"
msgstr "Gagal menyimpan opsi nilai"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4371
msgid "Destination directory is not writeable"
msgstr "Direktori tujuan tidak dapat ditulisi"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4374
msgid "Destination directory does not exist"
msgstr "Direktori tujuan sudah tidak ada"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4377
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4386
msgid "Failed to move selected tracks"
msgstr "Gagal menghapus berkas yang terpilih"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4396
msgid "Following files were moved successfully"
msgstr "Berkas yang dimaksud telah sukses dipindahkan"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4398
msgid "Following files were NOT moved"
msgstr "Berkas yang dimaksud GAGAL dipindahkan"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4481
msgid "There is no compatible file in {p} or any of its sub directories"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4492
msgid "Failed to recursively add directory"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4513
msgid "Directory {p} has been removed"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4524
msgid "Failed to remove directory"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5037
msgid "Public link to the track"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5041
msgid "Public link to the folder"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5255
msgid "Select at least one track"
msgstr "Pilih setidaknya satu berkas"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5259
msgid "Destination folder"
msgstr "Folder tujuan"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5262
msgid "Origin and destination directories must be different"
msgstr "Asal dan tujuan petunjuk harus berbeda"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5287
msgid "Are you sure you want to delete the track {name} ?"
msgstr "Apakah anda yakin ingin menghapus berkas {name} ?"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5290
msgid "Confirm track deletion"
msgstr "Konfirmasi penghapusan berkas"

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5309
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:45
msgid "Add directory"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5320
msgid "Add directory recursively"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:187
msgid "better in"
msgstr "lebih baik"

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:192
msgid "worse in"
msgstr "lebih buruk"

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:202
msgid "Divergence details"
msgstr "Perbedaan rincian"

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:210
msgid "Divergence distance"
msgstr "Perbedaan jarak"

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:214
msgid "is shorter than"
msgstr "apakah lebih cepat dibandingkan dengan"

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:224
msgid "is longer than"
msgstr "apakah lebih lama dibandingkan dengan"

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:240
msgid "Divergence time"
msgstr "Perbedaan waktu"

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:244
msgid "is quicker than"
msgstr "apakah lebih cepat daripada"

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:253
msgid "is slower than"
msgstr "apakah lebih lambat dibandingkan dengan"

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:273
msgid "is less than"
msgstr "apakah kurang dari"

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:283
msgid "is more than"
msgstr "apakah lebih dari"

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:295
msgid "There is no divergence here"
msgstr "Tidak ada perbedaan di sini"

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:299
msgid "Segment details"
msgstr "Rincian bagian"

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:301
msgid "Segment id"
msgstr "Bagian id"

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:302
msgid "From"
msgstr "Dari"

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:305
msgid "To"
msgstr "Ke"

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:320
msgid "Time"
msgstr "Waktu"

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:382
msgid "Click on a track line to get details on the section"
msgstr "Klik pada garis berkas untuk mendapatkan rincian pada bagian"

#: /var/www/html/n16/apps/gpxpod/js/jquery.colorbox-min.js:6
msgid "LoadedContent"
msgid_plural "width:0; height:0; overflow:hidden; visibility:hidden"
msgstr[0] "LoadedContent\n"
"width:0; height:0; overflow:hidden; visibility:hidden"

#: /var/www/html/n16/apps/gpxpod/js/leaflet.js:5
msgid "left"
msgstr "kiri"

#: /var/www/html/n16/apps/gpxpod/js/leaflet.js:5
msgid "right"
msgstr "kanan"

#: /var/www/html/n16/apps/gpxpod/specialAppInfoFakeDummyForL10nScript.php:3
msgid " "
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:4
msgid "Folder and tracks selection"
msgstr "Folder dan berkas pilihan"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:5
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:398
msgid "Settings and extra actions"
msgstr "Pengaturan dan tindakan lebih"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:6
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:640
msgid "About GpxPod"
msgstr "Tentang GpxPod"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:22
msgid "Choose a folder"
msgstr "Pilih folder"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:40
msgid "Reload and analyze all files in current folder"
msgstr "Memuat kembali dan menganalisa semua berkas dalam folder yang sekarang"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:41
msgid "Reload current folder"
msgstr "Memuat kembali folder yang sekarang"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:44
msgid "Add directories recursively"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:46
msgid "Delete current directory"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:52
msgid "There is no directory in your list"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:55
msgid "Add one to be able to see tracks"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:62
msgid "Options"
msgstr "Pilihan"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:70
msgid "Map options"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:74
msgid "Display markers"
msgstr "Tampilkan Penanda"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:77
msgid "Show pictures markers"
msgstr "Tampilkan spidol gambar"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:79
msgid "Only pictures with EXIF geolocation data are displayed"
msgstr "Hanya gambar dengan EXIF geolocation data ditampilkan"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:83
msgid "Show pictures"
msgstr "Menampilkan gambar"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:86
msgid "With this disabled, public page link will include option to hide sidebar"
msgstr "Dengan ini dinonaktifkan, link halaman umum akan mencakup pilihan untuk menyembunyikan sidebar"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:90
msgid "Enable sidebar in public pages"
msgstr "Mengaktifkan sidebar di halaman-halaman umum"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:93
msgid "Open info popup when a track is drawn"
msgstr "Info buka pop-up ketika lintasan yang ditarik"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:96
msgid "Auto-popup"
msgstr "Auto-popup"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:99
msgid "If enabled :"
msgstr "Jika diaktifkan :"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:101
msgid "Zoom on track when it is drawn"
msgstr "Zoom pada jalur adalah jika digambar"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:103
msgid "Zoom to show all track markers when selecting a folder"
msgstr "Zoom untuk menunjukkan semua melacak spidol ketika memilih folder"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:105
msgid "If disabled :"
msgstr "Jika diaktifkan:"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:107
msgid "Do nothing when a track is drawn"
msgstr "Melakukan apa-apa ketika lintasan yang ditarik"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:109
msgid "Reset zoom to world view when selecting a folder"
msgstr "Reset zoom untuk pandangan dunia ketika memilih folder"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:112
msgid "Auto-zoom"
msgstr "Pembesar otomatis"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:114
msgid "Display elevation or speed chart when a track is drawn"
msgstr "Menampilkan grafik kecepatan atau elevasi saat lintasan yang ditarik"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:118
msgid "Display chart"
msgstr "Tampilan grafik"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:121
msgid "Timezone"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:126
msgid "Measuring units"
msgstr "Unit pengukuran"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:128
msgid "Metric"
msgstr "Metrik"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:129
msgid "English"
msgstr "Bahasa Inggris"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:130
msgid "Nautical"
msgstr "Bahari"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:136
msgid "Track drawing options"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:137
msgid "Use symbols defined in the gpx file"
msgstr "Gunakan simbol yang didefinisikan dalam file gpx"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:141
msgid "Gpx symbols"
msgstr "%@ Simbol"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:144
msgid "Draw black borders around track lines"
msgstr "Menggambar hitam perbatasan di sekitar trek baris"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:148
msgid "Line borders"
msgstr "Garis perbatasan"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:150
msgid "Track line width in pixels"
msgstr "Melacak garis lebar pixel"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:152
msgid "Line width"
msgstr "Lebar garis"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:160
msgid "Display routes points"
msgstr "Tampilkan poin rute"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:163
msgid "Show direction arrows along lines"
msgstr "Menunjukkan arah panah sepanjang garis"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:167
msgid "Direction arrows"
msgstr "Arah Panah"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:172
msgid "track+waypoints"
msgstr "jalur+titikarah"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:173
msgid "track"
msgstr "jalur"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:174
msgid "waypoints"
msgstr "waypoint"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:179
msgid "Waypoint style"
msgstr "Gaya waypoint"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:185
msgid "Tooltip"
msgstr "Keterangan alat"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:187
msgid "on hover"
msgstr "di hover"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:188
msgid "permanent"
msgstr "permanen"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:194
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:197
msgid "Enables tracks coloring by the chosen criteria"
msgstr "Mengaktifkan pewarnaan jalur/lintasan dengan kriteria yang dipilih"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:195
msgid "Color tracks by"
msgstr "Jalur warna oleh"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:198
msgid "none"
msgstr "tidak ada"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:199
msgid "speed"
msgstr "kecepatan"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:200
msgid "elevation"
msgstr "ketinggian"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:201
msgid "pace"
msgstr "kecepatan"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:202
msgid "extension"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:208
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:211
msgid "Enables tracks coloring by the chosen extension value"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:209
msgid "Color tracks by extension value"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:215
msgid "IGC elevation track"
msgstr "Jalur elevasi IGC"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:217
msgid "Both GNSS and pressure"
msgstr "Baik GNSS maupun tekanan"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:218
msgid "Pressure"
msgstr "Tekanan"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:219
msgid "GNSS"
msgstr "GNSS"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:225
msgid "Table options"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:226
msgid "Table only shows tracks that are inside current map view"
msgstr "Tabel hanya menunjukkan trek yang di dalam tampilan peta saat ini"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:230
msgid "Dynamic table"
msgstr "Tiles dinamis"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:233
msgid "For slow connections or if you have huge files, a simplified version is shown when hover"
msgstr "Untuk koneksi yang lambat atau jika Anda memiliki file besar, versi sederhana yang ditunjukkan ketika hover"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:237
msgid "Simplified previews"
msgstr "Preview disederhanakan"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:241
msgid "Enables transparency when hover on table rows to display track overviews"
msgstr "Memungkinkan transparansi ketika mengarahkan pada baris tabel untuk menampilkan lagu Ikhtisar"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:245
msgid "Transparency"
msgstr "Opasitas"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:250
msgid "Storage exploration options"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:255
msgid "Display tracks recursively in selected folder"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:262
msgid "Display shared folders/files"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:269
msgid "Explore external storages"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:276
msgid "Display folders containing pictures only"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:281
msgid "Effective on future actions"
msgstr "Efektif untuk tindakan di masa depan"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:291
msgid "what determines if a track is shown in the table :"
msgstr "apa yang menentukan jika lagu yang ditampilkan dalam tabel:"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:293
msgid "crosses : at least one track point is inside current view"
msgstr "melintasi: titik setidaknya satu lagu yang ada di dalam tampilan aktif"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:295
msgid "begins : beginning point marker is inside current view"
msgstr "mulai: awal titik marker adalah pandangan saat ini dalam"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:297
msgid "track N,S,E,W bounds intersect current view bounds square"
msgstr "melacak N, S, E, batas-batas W berpotongan saat ini lihat batas persegi"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:301
msgid "List tracks that"
msgstr "Daftar trek yang"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:304
msgid "cross current view"
msgstr "cross tampilan aktif"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:305
msgid "begin in current view"
msgstr "memulai dalam tampilan aktif"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:306
msgid "have N,S,E,W bounds crossing current view"
msgstr "mempunyai ikatan N,S,E,W meyilang gambar yang aktif"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:311
msgid "Select visible"
msgstr "Pilih semua yang terlihat"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:314
msgid "Deselect all"
msgstr "Batalkan semua"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:317
msgid "Deselect visible"
msgstr "Pilih semua yang terlihat"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:320
msgid "Delete selected"
msgstr "Hapus yang dipilih"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:323
msgid "Move selected tracks to"
msgstr "Memindahkan daftar yang telah terpilih ke"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:327
msgid "Hide elevation profile"
msgstr "Menyembunyikan profil ketinggian"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:331
msgid "Compare selected tracks"
msgstr "Membandingkan versi yang dipilih"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:402
msgid "Filters"
msgstr "Penyaring"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:405
msgid "Clear"
msgstr "Bersih"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:409
msgid "Apply"
msgstr "Terapkan"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:417
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:422
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:427
msgid "min"
msgstr "mnt"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:418
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:423
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:428
msgid "max"
msgstr "maks"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:435
msgid "Custom tile servers"
msgstr "Kustom ubin server"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:438
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:478
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:521
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:566
msgid "Server name"
msgstr "Nama server"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:439
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:479
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:522
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:567
msgid "For example : my custom server"
msgstr "Sebagai contoh: server kustom saya"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:440
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:480
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:523
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:568
msgid "Server url"
msgstr "Url Server"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:441
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:524
msgid "For example : http://tile.server.org/cycle/{z}/{x}/{y}.png"
msgstr "Sebagai contoh : http//tile.server.org/cycle/{z}/{x}/{y}.png"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:442
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:482
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:525
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:570
msgid "Min zoom (1-20)"
msgstr "Min Zoom (1-20)"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:444
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:484
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:527
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:572
msgid "Max zoom (1-20)"
msgstr "Max zoom (1-20)"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:446
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:490
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:535
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:584
msgid "Add"
msgstr "Tambahkan"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:449
msgid "Your tile servers"
msgstr "Server ubin Anda"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:475
msgid "Custom overlay tile servers"
msgstr "Tampilan kustom ubin server"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:481
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:569
msgid "For example : http://overlay.server.org/cycle/{z}/{x}/{y}.png"
msgstr "Sebagai contoh: http://overlay.server.org/cycle/ {z} / {x} / {y} png"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:486
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:574
msgid "Transparent"
msgstr "Transparan"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:488
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:576
msgid "Opacity (0.0-1.0)"
msgstr "Opacity (0.0-1.0)"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:493
msgid "Your overlay tile servers"
msgstr "Tampilan ubin server Anda"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:518
msgid "Custom WMS tile servers"
msgstr "Kustom WMS ubin server"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:529
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:578
msgid "Format"
msgstr "Format"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:531
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:580
msgid "WMS version"
msgstr "Versi WMS"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:533
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:582
msgid "Layers to display"
msgstr "Lapisan untuk ditampilkan"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:538
msgid "Your WMS tile servers"
msgstr "Ubin server WMS Anda"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:563
msgid "Custom WMS overlay servers"
msgstr "Kustom tampilan server WMS"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:587
msgid "Your WMS overlay tile servers"
msgstr "Tampilan ubin server WMS Anda"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:616
msgid "Clean files or database"
msgstr "File bersih atau database"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:617
msgid "Delete all '.marker' and '.geojson' files"
msgstr "Menghapus semua file '.marker' dan '.geojson'"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:618
msgid "Delete '.markers' and '.geojson' files corresponding to existing gpx files"
msgstr "Hapus file '.markers' dan '.geojson' yang sesuai dengan file gpx ada"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:619
msgid "Metadata will be generated again on folder load"
msgstr "Metadata akan dihasilkan lagi pada folder beban"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:619
msgid "Delete metadata for all tracks in the database (distance, duration, average speed...)"
msgstr "Menghapus metadata untuk semua trek dalam database (jarak, durasi, kecepatan rata-rata...)"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:624
msgid "deleting"
msgstr "menghapus"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:631
msgid "Append \"&track=all\" to the link to display all tracks when public folder page loads."
msgstr "Menambahkan \"& melacak = semua\" untuk link untuk menampilkan semua trek ketika beban halaman folder publik."

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:642
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:143
msgid "Shortcuts"
msgstr "Pintasan"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:644
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:145
msgid "toggle sidebar"
msgstr "toggle sidebar"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:645
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:146
msgid "toggle minimap"
msgstr "minimap beralih"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:648
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:150
msgid "Features"
msgstr "Fitur-fitur"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:711
msgid "Documentation"
msgstr "Dokumentasi"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:719
msgid "Source management"
msgstr "Sumber manajemen"

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:733
msgid "Authors"
msgstr "Pengembang"

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:12
msgid "Upload gpx files to compare"
msgstr "Mengunggah file gpx untuk membandingkan"

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:27
msgid "Compare"
msgstr "Membandingkan"

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:48
msgid "File pair to compare"
msgstr "Pasangan berkas untuk membandingkan"

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:60
msgid "and"
msgstr "dan"

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:67
msgid "Color by"
msgstr "Warna oleh"

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:71
msgid "time"
msgstr "waktu"

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:77
msgid "cumulative elevation gain"
msgstr "kenaikan elevasi kumulatif"

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:99
msgid "Global stats on loaded tracks"
msgstr "Statistik global pada berkas yang dimuat"

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:105
msgid "stat name / track name"
msgstr "stat nama / nama berkas"

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:125
msgid "Number of points"
msgstr "Jumlah poin"

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:141
msgid "About comparison"
msgstr "Tentang perbandingan"

